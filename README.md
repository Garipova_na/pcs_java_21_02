# Курс "Введение в разработку корпоративных систем на Java"

* [Домашние задания](Homeworks.md)

---

* [Рекомендации по работе с Java](Additional/Java_use.md)

---

## 01. Введение в специальность. Основы информатики - архитектура компьютера,  системы счисления

* [01. Intro](Projects/01.%20Intro)

## 02. Разработка ПО для ЭВМ. Модели и методологии разработки ПО

* [02. Software Development](Projects/02.%20Software%20Development)

## 03. Языки программирования, cтруктуры управления, типы данных, построение алгоритмов

* [03. Algorithms](Projects/03.%20Algorithms)

## 04. Инфраструктура Java, первая программа, массивы

* [04. Java](Projects/04.%20Java)

## 05. Git

* [05. Git](Projects/05.%20Git)

## 06. Функции и процедуры

* [06. Functions and Procedures](Projects/06.%20Functions%20and%20Procedures)

## 07. Алгоритмы и оценка сложности

* [07. Algorithms and complexity](Projects/07.%20Algorithms%20and%20complexity)

## 08. ООП

* [08. OOP](Projects/08.%20OOP)

## 09. Наследование и полиморфизм

* [09. Inheritance](Projects/09.%20Inheritance)

## 10. Абстрактные классы и интерфейсы

* [10. Abstract Classes And Interfaces](Projects/10.%20Abstract%20Classes%20And%20Interfaces)

## 11. Статические члены классов

* [11. Static Members](Projects/11.%20Static%20Members)

## 12. Внутренние и вложенные классы

* [12. Nested Classes](Projects/12.%20Nested%20Classes)

## 13. Анонимные классы и лямбда выражения

* [13. Anonymous classes and lambda expressions](Projects/13.%20Anonymous%20classes%20and%20lambda%20expressions)

## 14. Классы Object и String

* [14. Object and String](Projects/14.%20Object%20and%20String)